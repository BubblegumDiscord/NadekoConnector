FROM node:carbon
WORKDIR /bot
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
volume /bot
ENTRYPOINT ["/tini", "--"]
CMD ["node", "server.js"]
