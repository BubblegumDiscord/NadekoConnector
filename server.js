const express = require('express')
const jwt = require("jsonwebtoken");
const config = require("./config.json");
const app = express()
const axios = require("axios");
console.log("Boot 2!")
app.get('/getbal/:token', async (req, res) => {
    try {
        var j = jwt.verify(req.params.token, config.jwt_secret);
        var uid = j.uid;
        var tI = j.timeIssued;
	    
        if (tI < (Date.now() - (1000 * 5))) return res.end("{\"error\": \"Your token was not valid\"}");
        var response = await axios.get("http://nadeko:9000/api", {
            params: {
                method: "getbal",
                id: uid
            }})
	
	console.log("Retrieved bal for " + uid + ", " + response.data)
        res.end(JSON.stringify({

            balance: Number.parseInt(response.data)
        }))

    } catch (e) {
        res.end("{\"error\": \"Your token was not valid\"}")
    }
})
app.get('/setbal/:token', (req, res) => {
    try {
        var j = jwt.verify(req.params.token, config.jwt_secret);
        var uid = j.uid;
        var tI = j.timeIssued;
        if (tI < (Date.now() - (1000 * 5))) return res.end("{\"error\": \"Your token was not valid\"}");
        axios.get("http://nadeko:9000/api", {
            params: {
                method: "setbal",
                id: uid,
                amount: j.amount,
                sendMessage: j.sendMessage ? j.sendMessage : undefined,
                reason: j.reason ? j.reason : undefined
            }}).then(x => res.end("{\"status\": \"ok\"}"))
    } catch (e) {
        res.end("{\"error\": \"Your token was not valid\"}")
    }
})

app.listen(3000, () => console.log('Listening on port 3000!'))
